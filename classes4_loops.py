names = ['Adrian', 'Kasia', 'Tomek', 'Marta']
temperatures_in_celsius = [10.2, 20.4, 30, 2, 7, 11, -8, 18]

for name in names:
    print(f'Hello {name}!')


def print_temperatures_in_fahrenheit(temperates_in_celsius):
    for temperature in temperates_in_celsius:
        temp_in_fahrenheit = 1.6 * temperature + 32
        print(f'{temperature} in Celsius is equal to {temp_in_fahrenheit} Fahrenheit')


print_temperatures_in_fahrenheit(temperatures_in_celsius)
