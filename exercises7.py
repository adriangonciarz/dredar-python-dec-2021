class Car:
    def __init__(self, brand):
        self.brand = brand
        self.age = 0
        self.total_distance = 0
        self.amount_of_gas = 0

    def fill_gas(self, gas_amount):
        self.amount_of_gas += gas_amount

    def drive(self, distance_in_kilometers):
        # Calculate how much gas we need to drive this distance
        required_gas = (distance_in_kilometers / 100) * 10
        # Only change the values if drive is possible
        if required_gas <= self.amount_of_gas:
            self.amount_of_gas -= required_gas
            self.total_distance += distance_in_kilometers


def test_new_car_initial_values():
    car = Car('Toyota')
    assert car.total_distance == 0
    assert car.age == 0
    assert car.amount_of_gas == 0


def test_car_should_fill_up_gas():
    car = Car('Toyota')
    car.fill_gas(10)
    assert car.amount_of_gas == 10


def test_car_should_not_increase_distance_when_driven_without_gas():
    car = Car('Toyota')
    car.drive(100)
    assert car.total_distance == 0


def test_car_should_increase_distance_and_decrease_gas_amount_when_driven():
    car = Car('Toyota')
    car.fill_gas(20)
    car.drive(100)
    assert car.total_distance == 100
    assert car.amount_of_gas == 10
