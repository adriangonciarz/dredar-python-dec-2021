a = 3

if a > 0:
    print('a is positive')


def is_speed_allowed_in_builtup_area(speed):
    if speed > 50:
        return False
    else:
        return True


def is_it_hot(temperature):
    if temperature > 30:
        print("It's HOT!")
    elif temperature > 10:
        print("It's OK.")
    elif temperature > 0:
        print("It's cold but not freezing.")
    else:
        print("It's FREEZING :(((!")


def how_good_is_score(score):
    if score > 5.0 or score < 2.0:
        return 'N/A'
    elif score >= 4.5:
        return 'Bardzo dobry'
    elif score >= 4.0:
        return 'Dobry'
    elif score >= 3.0:
        return 'Dostateczny'
    else:
        return 'Niedostateczny'


print(is_speed_allowed_in_builtup_area(30))
is_it_hot(0)
is_it_hot(10)
is_it_hot(30)
is_it_hot(31)
print(how_good_is_score(4.4))
print(how_good_is_score(15.1))
