import datetime

countries_map = {
    'Japan': ('Toyota', 'Suzuki', 'Subaru'),
    'Germany': ('BMW', 'Audi', 'Volkswagen'),
    'South Korea': ('Hyundai',)
}
original_cars = [
    {'brand': 'Toyota', 'year': 1998, 'name': 'Camry'},
    {'brand': 'BMW', 'year': 2008, 'name': '330'},
    {'brand': 'Audi', 'year': 2020, 'name': 'RS3'},
    {'brand': 'Hyundai', 'year': 2017, 'name': 'i30'},
]


def get_name_of_car(car_dictionary):
    brand = car_dictionary['brand']
    name = car_dictionary['name']
    return f'{brand} {name}'


def get_age_of_car(car_dictionary):
    this_year = datetime.datetime.now().year
    age = this_year - car_dictionary['year']
    return age


def get_country_of_car(car_dictionary):
    brand = car_dictionary['brand']
    for country, brands in countries_map.items():
        if brand in brands:
            return country


def convert_car(car_dictionary):
    return {
        'name': get_name_of_car(car_dictionary),
        'country': get_country_of_car(car_dictionary),
        'age': get_age_of_car(car_dictionary)
    }


def convert_cars_data(cars_list):
    converted_cars = []
    for car in cars_list:
        new_car = convert_car(car)
        converted_cars.append(new_car)
    return converted_cars


print(convert_cars_data(original_cars))
