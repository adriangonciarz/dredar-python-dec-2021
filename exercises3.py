# Exercise 1
primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
number_of_elements = len(primes)
print(f'Number of elements equals {number_of_elements}.')

primes.append(31)
print(f'31 appended to the list: {primes}')

first_four_elements = primes[0:4]
print(f'The first four elements are: {first_four_elements}.')

sum_of_elements = sum(primes)
number_of_elements = len(primes)
average_of_elements = round(sum_of_elements / number_of_elements, 1)
print(average_of_elements)


# Exercise 2
def sort_list(users_list):
    result = sorted(users_list)
    return result


def print_first_element_of_sorted_list(users_list):
    result = sort_list(users_list)[0]
    print(f'Exercise 2. The first element of the sorted list is: {result}.')


def print_last_three_elements_of_sorted_list(users_list):
    result = sort_list(users_list)[-3:]
    print(f'Exercise 2. The last 3 elements of the sorted list are: {result}.')


random_numbers = [22, 3.3, -2, 5, 701, 42, -120, 35, 69.9, 123, -444, 0.0, 0]

print(f'Exercise 2. Sorted list: {sort_list(random_numbers)}.')
print_first_element_of_sorted_list(random_numbers)
print_last_three_elements_of_sorted_list(random_numbers)


# Exercise 3
def multiply_three_number(a, b, c):
    return a * b * c


multiplication_example1 = multiply_three_number(1, 2, 3)
multiplication_example2 = multiply_three_number(5, 11, 4)
print("results of multiplication:", multiplication_example1, multiplication_example2, sep="\n")


# Exercise 4
def can_buy_alcohol(age):
    return age >= 18


print(f'Can 7 year old person buy alcohol? {can_buy_alcohol(7)}')
