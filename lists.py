movies = ['Dune', 'Blade Runner', 'Stalker', 'Solaris', 'Lost Highway']
first_movie = movies[0]
print(first_movie)
print(movies[2])

number_of_movies = len(movies)
print(number_of_movies)

last_movie_long_way = movies[number_of_movies-1]
last_movie = movies[-1]

movies.append('Star Wars: Jedi Return')
print(movies[-1])
print(movies)
print(len(movies))

