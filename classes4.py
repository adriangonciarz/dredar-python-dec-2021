pet = {
    'name': 'Bonnie',
    'age': 9,
    'type': 'Bernese Shepherd',
    'male': False
}
print(pet['name'])
pet['age'] = 10
print(pet['age'])
pet['owner'] = 'Mama Adriana'
print(pet)

friend = {
    'name': 'Gandalf',
    'age': 647,
    'hobbies': ['smoking a pipe', 'travelling']
}
print(friend['hobbies'][-1])
friend['hobbies'].append('Fighting Nazguls')
print(friend)
