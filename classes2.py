first_name = 'Adrian'
last_name = 'Gonciarz'
phone_number = '+48000000000'
email = 'adrian.gonciarz@example.com'

print('name:', first_name, last_name)
print('phone:', phone_number)
print('email:', email)

bio = 'My name is ' + first_name + ' ' + last_name
print(bio)

full_bio = f'My name is {first_name} {last_name}.\nMy phone number is {phone_number}.\nMy email is {email}'
print(full_bio)

radius = 13
area = 3.14 * radius ** 2
print(area)

is_wednesday = True
is_sunny = True
is_sunny_wednesday = is_sunny and is_wednesday
print('Is it sunny wednesday?', is_sunny_wednesday)

is_saturday = True
is_sunday = False
is_weekend = is_sunday or is_saturday
print('Is it weekend already?!!??!?!', is_weekend)

age = 18
is_adult = age >= 18
print(is_adult)

temperature = 0.1
is_water_freezing = temperature <= 0

time_of_day = '12:00'
is_radio_transmission = time_of_day == '12:00'
print('Is it on the radio?', is_radio_transmission)



