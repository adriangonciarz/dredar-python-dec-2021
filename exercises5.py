# Exercise 1
def describe_coordinates(lat, lon):
    if lat == 0:
        print("You are on the equator!")
    elif lat == 90:
        print("You are on the North Pole, Ho ho ho!")
    elif lat == -90:
        print("You are on the South Pole")
    elif lat == 52 and lon == 21:
        print("Welcome to Warsaw")
    else:
        print("Welcome to planet Earth")


# Exercise 2
numbers = [-12, 4, 5, 77, 890, -2, -1000, 41, 42, 791, -230, 435]

for number in numbers:
    if number > 11:
        print('Greater than 11:', number)
    if number % 2 == 0:
        print('Even number:', number)
    if number < -100:
        print('Less than -100:', number)

print('List numbers:', numbers)


# Exercise 3
def find_elements_divisible_by_3(numbers_list):
    output_list = []
    for number in numbers_list:
        if number % 3 == 0:
            output_list.append(number)
    return output_list


print('Elements of list numbers divisible by 3:', find_elements_divisible_by_3(numbers))


# Exercise 4
def mark_list_elements_is_even(numbers_list):
    output_list = []
    for number in numbers_list:
        is_even = number % 2 == 0
        output_list.append(is_even)
    return output_list


print('List numbers marked as even number:', mark_list_elements_is_even(numbers))
