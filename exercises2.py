# Exercise 1
first_name = 'Basia'
last_name = 'Ska'
email = 'fake@email.com'
phone = '0123456'
age = 100
bio = f'My name is {first_name} {last_name}.\nI\'m {age} years old.\nYou can contact me via email: {email} or phone: {phone}.'
print(bio)

# Exercise 1A
first_name = 'Szymon'
last_name = 'Ptasznik'
email = 'szp@gugle.com'
phone = '+48123456'
age = 40
personal_data = f'My name is {first_name} {last_name}.'
my_age = f'I am {age} years old.'
contact = f'You can contact me via email: {email} or phone: {phone}.'
print(personal_data, my_age, contact, sep='\n')

# Exercise 2
pi = 3.1415
r = 2
circumference = 2 * pi * r
area = pi * r ** 2


def circle_circumference(radius):
    return 2 * pi * radius


my_circle_circumference = circle_circumference(r)
print(my_circle_circumference)

circumference_1 = circle_circumference(1)
circumference_10 = circle_circumference(10)
circumference_30 = circle_circumference(30)

# Exercise 3
left = 5
right = 13


def triangle_area(side, height):
    return side * height / 2


def triangle_hypotenuse(a, b):
    value = (a ** 2 + b ** 2) ** 0.5
    value_rounded = round(value, 2)
    return value_rounded


triangle_area_1 = triangle_area(5, 4)
triangle_area_2 = triangle_area(3, 3)
print(triangle_area_1)
print(triangle_area_2)

# First value from task
my_triangle_area = triangle_area(left, right)
my_triangle_hypotenuse = triangle_hypotenuse(left, right)
my_triangle_circumference = left + right + my_triangle_hypotenuse

output = f'Area: {my_triangle_area}\nHypotenuse: {my_triangle_hypotenuse}\nCircumference: {my_triangle_circumference}'
print(output)


# Exercise 4
def can_use_promotion(age, sex):
    is_senior = age > 65
    is_woman = sex == 'f' and (age <= 16 or age > 45)
    is_man = sex == 'm' and 20 <= age <= 40
    return is_senior or is_woman or is_man


print('Woman, 55', can_use_promotion(55, 'f'))
print('Man, 55', can_use_promotion(55, 'm'))
