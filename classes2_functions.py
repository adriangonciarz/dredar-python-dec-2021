def say_hello():
    print('Hello!')


def greet(name):
    print(f'Hello dear {name}!')


def sum_of_two_numbers(a, b):
    return a + b


say_hello()
greet('Adrian')
greet('Dawid')

sum_of_some_numbers = sum_of_two_numbers(11, 14)
print(sum_of_some_numbers)
