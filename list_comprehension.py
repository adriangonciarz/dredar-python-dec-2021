names = ['adrian', 'dawid', 'basia', 'marta']

capitalized_names = []
for idx, name in enumerate(names):
    print(idx)
    name_capitalized = name.capitalize()
    capitalized_names.append(name_capitalized)

print(capitalized_names)

# List comprehension
capitalized_names_2 = [name.capitalize() for name in names]
