from parser.parser import convert_amount_string_to_integer


def test_convert_large_amount_to_integer():
    test_string = '$560,321'
    actual_value = convert_amount_string_to_integer(test_string)
    assert actual_value == 560321


def test_convert_small_amount_to_integer():
    test_string = '$21'
    actual_value = convert_amount_string_to_integer(test_string)
    assert actual_value == 21
