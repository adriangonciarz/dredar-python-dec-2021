import requests
from bs4 import BeautifulSoup
import csv


def convert_amount_string_to_integer(amount):
    return int(amount.replace('$', '').replace(',', ''))


def convert_table_row_elements_to_data_list(table_rows):
    all_data = []
    for idx, row in enumerate(table_rows):
        row_data = row.find_all('td')

        # cleaned_row = []
        # for rd in row_data:
        #     cleaned_row.append(rd.text)
        if idx > 0:
            cleaned_row = [convert_amount_string_to_integer(rd.text) for rd in row_data]
        else:
            cleaned_row = [rd.text for rd in row_data]
        all_data.append(cleaned_row)
    return all_data


def write_all_data_list_to_csv(all_data, filename):
    field_names = all_data[0]
    data_rows = all_data[1:]

    with open(filename, 'w') as file:
        write = csv.writer(file)

        write.writerow(field_names)
        write.writerows(data_rows)


def read_url_to_beautiful_soup_page(url):
    req = requests.get(url)
    page = BeautifulSoup(req.content, 'html.parser')
    return page


"""
Wczytaj stronę html
"""

page_url = 'http://agonciarz-public.s3-website.eu-central-1.amazonaws.com/'
page = read_url_to_beautiful_soup_page(page_url)

"""
Znalezc odpowiednie wiersze (tr) i wczytac dane do jakiejs listy
W pierwszym wierszu wewnatrz tbody beda naglowki, w kazdym kolejnym wartosci -> kazda wartosc siedzi w elemencie td
Każdą wartość numeryczną muszę wyczyścić -> usunąć znak dolara i przecinek
Moja lista moze wygladac tak:
[
    ['Income','Cost','Tax','Profit'],
    [1000,30,123,90],
    [400,320,123,90],
    ...
]
"""

table_rows = page.tbody.find_all('tr')
all_data = convert_table_row_elements_to_data_list(table_rows)

"""
Wyeksportowac dane z listy do pliku csv, gdzie nagłówki są pierwszym wierszem,
a kazda kolejna wartosc to nowa linia

Income,Cost,Tax,Profit
200000,50000,15000,135000
90000,45000,20000,17000
900000,20000,320000,560000
"""
write_all_data_list_to_csv(all_data, 'output.csv')
