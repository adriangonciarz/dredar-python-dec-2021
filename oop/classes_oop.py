from oop.animals import Dog

dog1 = Dog('Stefan')
dog2 = Dog('Robert')
print(dog1.name)
print(dog2.name)
dog1.name = 'Azor'
dog2.name = 'Burek'
print(dog1.name)
print(dog2.name)
dog2.kind = 'Rottweiler'
dog1.bark()
dog1.print_info()
dog2.print_info()
