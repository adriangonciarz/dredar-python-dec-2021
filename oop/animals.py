class Dog:
    def __init__(self, name):
        self.name = name
        self.kind = 'German Shepherd'

    def bark(self):
        print(f'Woof! Woof! ({self.name})')

    def print_info(self):
        print(f'Name of dog: {self.name}. Kind of dog: {self.kind}')

