import random


class Player:
    def __init__(self, name, position, age):
        self.name = name
        self.age = age
        self.goals = 0
        self.field_number = 0
        self.cards = 0
        self.club = None
        self.position = position

    def score(self):
        if self.club:
            self.goals += 1
        else:
            print('Choose your club!')

    def change_position(self, new_position):
        self.position = new_position
        self.age += 2

    def change_club(self, new_club):
        self.club = new_club
        self.goals = 0

    def foul(self):
        marker = random.randint(0, 1)
        if marker:
            self.cards += 1


