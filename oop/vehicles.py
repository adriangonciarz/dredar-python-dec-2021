"""
Uzupelnij klase Car, tak aby miala 4 pola:
- brand (wymagane przy inicjalizacji)
- model (wymagane przy inicjalizacji)
- total_distance (niewymagane przy inicjalizacji, pocz. wartosc: 0)
- gas_amount (niewymagane przy inicjalizacji, pocz. wartosc: 5)

Utwórz samochód "Toyota Corolla"
Wydrukuj jego total_distance

Stwórz metodę drive() przyjmującą dystans do przejechania i zwiększającą wartość total_distance
Przejedz 500km i wydrukuj total_distance samochodu
"""


class Car:
    def __init__(self, brand, model):
        self.brand = brand
        self.model = model
        self.total_distance = 0
        self.gas_amount = 5

    def drive(self, distance):
        # self.total_distance = self.total_distance + distance
        self.total_distance += distance


class Bus(Car):
    def drive_to_school(self):
        self.drive(50)


class Truck(Car):
    def load_heavy_stuff(self):
        print('Truck is loaded')


car1 = Car('Toyota', 'Corolla')
car2 = Car('Hyundai', 'i30')
print(car1.total_distance)
print(car2.total_distance)

car1.drive(300)
car2.drive(50)

print(car1.total_distance)
print(car2.total_distance)

school_bus = Bus('Solaris', 'School')
school_bus.drive(30)
school_bus.drive_to_school()
print(school_bus.total_distance)

truck = Truck('Scania', 'XYT')
truck.drive(100)
truck.load_heavy_stuff()
