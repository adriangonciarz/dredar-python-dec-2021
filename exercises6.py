class Player:
    def __init__(self, name):
        self.name = name
        self.age = None
        self.goals = 0

    def score(self):
        self.goals += 1


if __name__ == '__main__':
    my_player = Player('Test Player')
    print(my_player.age)  # powinno dać None
    my_player.age = 23
    print(my_player.age)  # powinno dać 23
    print(my_player.goals)  # powinno dać 0
    my_player.score()
    my_player.score()
    print(my_player.goals)  # powinno dać 2
