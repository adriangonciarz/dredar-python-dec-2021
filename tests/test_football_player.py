from oop.football import Player


def test_initial_values_for_player():
    player = Player('test_name', 'goalkeeper', 67)
    assert player.name == 'test_name'
    assert player.position == 'goalkeeper'
    assert player.age == 67
    assert player.club is None
    assert player.field_number == 0
    assert player.goals == 0
    assert player.cards == 0


def test_score_without_club_should_not_increase_goals():
    player = Player('test_name', 'goalkeeper', 67)
    player.score()
    assert player.goals == 0


def test_score_with_club_should_increase_goals():
    player = Player('test_name', 'goalkeeper', 67)
    player.club = 'Juventus'
    player.score()
    assert player.goals == 1


def test_changing_position_should_increase_age():
    player = Player('test_name', 'goalkeeper', 33)
    player.change_position('defender')
    assert player.age == 35
    assert player.position == 'defender'


def test_changing_club_should_reset_goals():
    player = Player('test_name', 'goalkeeper', 33)
    player.club = 'FC Test'
    player.score()
    player.change_club('Ruch Chorzów')
    assert player.goals == 0
    assert player.club == 'Ruch Chorzów'


def test_multiple_fouls_should_yield_a_card():
    player = Player('test_name', 'goalkeeper', 33)
    for i in range(10):
        player.foul()
    assert player.cards > 0
