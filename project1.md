# Projekt 1

### Początek
Zacznij od pliku `project1_initial.py`, spróbuj go uruchomić.

### Problem do rozwiązania
Mamy daną listę słowników opisujących samochody. Każdy samochód opsiuje słownik w postaci kluczy:
- brand (marka samochodu)
- model(model samochodu)
- year (rok produckji samochodu)
Chcemy przekonwertować tę listę na nową, która będzie dla każdego samochodu zawierać nowy słownik z innymi danymi:
- name (marka i model samochodu oddzielone spacją)
- age (wiek samochodu)
- country (kraj produkcji samochodu)

### Rowiązanie
Uzupałnij wszytskie miejsca w kodzie, gdzie umieściłęm komentarz. Musisz zastanowić się jaki argument dana funkcja przyjmuje oraz co ma zwracac.
W funkcji `convert_car()` musisz użyć dwóch poprzednio uzupełnionych funkcji. Następnie w pętli wewnątrz `convert_cars_data()` przekonwertuj każdy ze słowników i dopisz do nowej listy.
Na koniec skorzystaj z funkcji `convert_cars_data()` dla listy `original_cars` i wydrukuj nową listę.

Oczekiwany rezultat:
```
[{'model': 'Toyota Camry', 'country': 'Japan', 'age': 23}, {'model': 'BMW 330', 'country': 'Germany', 'age': 13}, {'model': 'Audi RS3', 'country': 'Germany', 'age': 1},{'model': 'Hyundai i30', 'country': 'South Korea', 'age': 4}]
```