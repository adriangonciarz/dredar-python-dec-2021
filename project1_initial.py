countries_map = {
    'Japan': ('Toyota', 'Suzuki', 'Subaru'),
    'Germany': ('BMW', 'Audi', 'Volkswagen'),
    'South Korea': ('Hyundai',)
}


def get_name_of_car(car_dictionary):
    # Fill the function to return model of a car as a string containing brand and model
    # Input argument is single car dictionary
    pass


def get_age_of_car(car_dictionary):
    # Fill the function to return age of a car
    # Input argument is single car dictionary
    pass


def get_country_of_car(car_dictionary):
    brand = car_dictionary['brand']
    for country, brands in countries_map.items():
        if brand in brands:
            return country


def convert_car(car_dictionary):
    return {
        'name': None,  # Use function for getting model of a car
        'country': get_country_of_car(car_dictionary),
        'age': None     # Use function for getting age of a car
    }


def convert_cars_data(cars_list):
    converted_cars = []
    for car in cars_list:
        # For each car in the list use function for converting car
        # And using it add converted car to converted_cars list
        pass
    return converted_cars


original_cars = [
    {'brand': 'Toyota', 'year': 1998, 'name': 'Camry'},
    {'brand': 'BMW', 'year': 2008, 'name': '330'},
    {'brand': 'Audi', 'year': 2020, 'name': 'RS3'},
    {'brand': 'Hyundai', 'year': 2017, 'name': 'i30'},
]

"""
Use convert_cars_data on the list above and print new list that should look like this:
[
    {'model': 'Toyota Camry', 'country': 'Japan', 'age': 23}, 
    {'model': 'BMW 330', 'country': 'Germany', 'age': 13}, 
    {'model': 'Audi RS3', 'country': 'Germany', 'age': 1}, 
    {'model': 'Hyundai i30', 'country': 'South Korea', 'age': 4}
]
"""
